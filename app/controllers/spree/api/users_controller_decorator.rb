module Spree
  module Api

      UsersController.class_eval do

        before_filter :check_for_api_key, :except => [:sign_in]

        def sign_in

          @user = Spree::User.find_by_email(params[:user][:email])

          if !@user.present?
            render 'spree/api/errors/invalid_credentials', status: 401
          elsif !@user.valid_password?(params[:user][:password])
            render 'spree/api/errors/invalid_credentials', status: 401
          else
            # @user.device_type = params[:user][:device_type]
            # if params[:user][:device_token]
            #   @user.device_token = params[:user][:device_token]
            # elsif params[:user][:registration_id]
            #    @user.registration_id = params[:user][:registration_id]
            # end
            @user.generate_spree_api_key!
            respond_with(@user, :status => 200)
          end
        end
      end
   end
end
