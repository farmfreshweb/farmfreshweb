class NewProductsEmail
  def self.run
    enterprise_products = []

    subscribed_enterprise_ids = FarmersMarketSubscriber.where(unsubscribed: false).map(&:enterprise_id).uniq
    enterprises = Enterprise.distributors_with_active_order_cycles.where(id: subscribed_enterprise_ids)
    enterprises.each do |enterprise|
      prev_order_cycle = OrderCycle.most_recently_closed.with_distributor(enterprise.id).first
      next if prev_order_cycle.nil?
      prev_products = prev_order_cycle.products_distributed_by(enterprise)
      active_order_cycle = OrderCycle.active.with_distributor(enterprise.id).first
      curr_products = active_order_cycle.products_distributed_by(enterprise)
      new_products = curr_products - prev_products
      enterprise_products << {
        enterprise: enterprise, products: new_products.uniq,
        order_cycle: active_order_cycle
      } if new_products.any?
    end

    FarmersMarketSubscriber.where(unsubscribed: false).group_by { |fms| fms.email }.each do |email, fmss|
      eids = fmss.map(&:enterprise_id)
      eps = enterprise_products.map { |ep| ep if eids.include?(ep[:enterprise].id) }.compact
      next unless eps.any?
      order_cycle = eps.find { |e| eids.include?(e[:enterprise].id) }[:order_cycle]
      SubscriberMailer.weekly(email, eps, order_cycle).deliver
    end
  end
end
