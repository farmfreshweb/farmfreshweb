class OrderCycleService
  def self.run
    order_cycles = OrderCycle.where(orders_close_at: 1.day.ago..Time.now)
    order_cycles.each do |order_cycle|
      order_cycle.distributors.each do |distributor|
        distributor.customers.each do |customer|
          customer.orders.where(created_at: (order_cycle.orders_open_at)..(order_cycle.orders_close_at)).each do |order|
            Spree::OrderMailer.customer_report(order, order_cycle).deliver if order.completed?
          end
        end
        order_cycle.suppliers.each do |supplier|
          orders = distributor.distributed_orders.where(order_cycle_id: order_cycle.id)
          line_items = orders.map(&:line_items).flatten.select { |li| li.product.supplier_id == supplier.id }
          if line_items.present?
            Spree::OrderMailer.farmer_pickup_report(supplier, distributor, order_cycle, line_items).deliver
          end
        end
      end
    end
  end
end
